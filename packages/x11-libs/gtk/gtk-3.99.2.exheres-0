# Copyright 2020 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'gtk+-2.13.2.ebuild' from Gentoo which is:
#    Copyright 1999-2008 Gentoo Foundation

require gtk

SLOT="4.0"
PLATFORMS="~amd64 ~x86"

LANGS="af am ang ar as ast az_IR az be@latin be bg bn_IN bn br bs ca ca@valencia crh cs cy da de dz
el en_CA en_GB en en@shaw eo es et eu fa fi fr ga gl gu he hi hr hu hy ia id io is it ja ka kg kk kn
ko ku lg li lt lv mai mi mk ml mn mr ms my nb nds ne nl nn nso oc or pa pl ps pt_BR pt ro ru rw si
sk sl sq sr@ije sr@latin sr sv ta te th tk tr tt ug uk ur uz@cyrillic uz vi wa xh yi zh_CN zh_HK
zh_TW"

MYOPTIONS="
    cloudprint [[ description = [ enable google cloudprint compatibility ] ]]
    colord [[
        description = [ color profiling support for the CUPS printing backend ]
        requires = [ cups ]
    ]]
    cups
    ffmpeg
    gstreamer
    gtk-doc
    tracker [[ description = [ Search engine based on tracker ] ]]
    vulkan [[ description = [ Support for the Vulkan API ] ]]
    wayland
    X
    ( wayland X ) [[ number-selected = at-least-one ]]
    ( linguas: ${LANGS} )
"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        sys-devel/gettext[>=0.19.7]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.99] )
        X? ( x11-proto/xorgproto )
        vulkan? ( sys-libs/vulkan-headers )
        wayland? ( sys-libs/wayland-protocols[>=1.20] )
    build+run:
        dev-libs/fribidi
        dev-libs/glib:2[>=2.65.0]
        dev-libs/libepoxy[>=1.4][X?]
        gnome-desktop/gobject-introspection:1[>=1.39.0]
        x11-dri/mesa[X?][wayland=]
        x11-libs/pango[>=1.47.0][gobject-introspection]
        x11-libs/cairo[>=1.14.0][X?]
        x11-libs/gdk-pixbuf:2.0[>=2.30.0][gobject-introspection]
        x11-libs/graphene:1.0[>=1.9.1][gobject-introspection]
        x11-libs/harfbuzz[>=0.9]
        cloudprint? (
            core/json-glib
            net-libs/rest
        )
        colord? ( sys-apps/colord[>=0.1.9] )
        cups? ( net-print/cups[>=1.2] )
        ffmpeg? ( media/ffmpeg )
        gstreamer? ( media-plugins/gst-plugins-bad[>=1.12.3] )
        X? (
            media-libs/fontconfig
            x11-libs/libX11
            x11-libs/libXext
            x11-libs/libXinerama
            x11-libs/libXi
            x11-libs/libxkbcommon[>=0.2.0]
            x11-libs/libXrandr[>=1.5]
            x11-libs/libXcursor
            x11-libs/libXfixes
            x11-libs/libXcomposite
            x11-libs/libXdamage
        )
        tracker? ( app-pim/tracker:3.0 )
        vulkan? ( sys-libs/vulkan-loader[X?][wayland?] )
        wayland? (
            sys-libs/wayland[>=1.14.91]
        )
        !x11-libs/gtk+:4.0 [[
            description = [ Package renamed ]
            resolution = uninstall-blocked-before
        ]]
    post:
        x11-themes/hicolor-icon-theme
    recommendation:
        gnome-desktop/adwaita-icon-theme    [[
            description = [ Default icon theme, many themes may require it ]
        ]]
        gnome-desktop/gnome-themes-extra [[
            description = [ Default GTK themes, many themes may require them ]
        ]]
    suggestion:
        app-vim/gtk-syntax [[
            description = [ A collection of vim syntax files for various GTK+ C extensions ]
        ]]
        gnome-desktop/evince [[
            description = [ used for print preview functionality ]
        ]]
        cups? (
            net-dns/avahi [[
                description = [ used for mDNS printer discovery support ]
            ]]
        )
"

RESTRICT="test" # require X

MESON_SRC_CONFIGURE_PARAMS+=(
    '-Dbroadway-backend=false'
    '-Dwin32-backend=false'
    '-Dmacos-backend=false'

    '-Dcloudproviders=disabled'
    '-Dsysprof=disabled'

    '-Dman-pages=true'
    '-Dintrospection=true'

    '-Ddemos=true'
    '-Dbuild-tests=true'
    '-Dbuild-tests=true'
    '-Dinstall-tests=false'
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    colord
    tracker
    vulkan
    'X xinerama'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'X x11-backend'
    'wayland wayland-backend'

    'gtk-doc gtk_doc'
)

src_prepare() {
    meson_src_prepare
    edo rm -r subprojects/gtk-doc
}

src_configure() {
    local media_backends=()
    optionq ffmpeg && media_backends+=(ffmpeg)
    optionq gstreamer && media_backends+=(gstreamer)
    [[ -z "${media_backends[*]}" ]] && media_backends+=(none)

    local print_backends=(file)
    optionq cups && print_backends+=(cups)
    optionq cloudprint && print_backends+=(cloudprint)

    meson_src_configure \
        "-Dmedia=$(IFS=,; echo "${media_backends[*]}")" \
        "-Dprint=$(IFS=,; echo "${print_backends[*]}")"
}

src_install() {
    meson_src_install
    gtk_alternatives
    edo find "${IMAGE}" -type d -empty -delete
}
