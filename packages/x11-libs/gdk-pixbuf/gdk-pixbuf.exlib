# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require meson

export_exlib_phases src_prepare pkg_postinst pkg_postrm

SUMMARY="An image loading library"
HOMEPAGE="https://www.gnome.org/"

LICENCES="LGPL-2"
SLOT="2.0"
MYOPTIONS="
    jpeg2000 [[ description = [ Support for JPEG 2000 Part-1 images using libjasper ] ]]
    gobject-introspection
    gtk-doc
    tiff
    X [[ description = [ Build libX11 rendering helper library ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.20] )
    build+run:
        dev-libs/glib:2[>=2.48.0]
        media-libs/libpng:=
        x11-misc/shared-mime-info
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.3] )
        jpeg2000? ( media-libs/jasper )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        tiff? ( media-libs/tiff )
        X? ( x11-libs/libX11 )
"

MESON_SRC_CONFIGURE_PARAMS=(
    '-Dgio_sniffing=true'
    '-Dinstalled_tests=false'
    '-Djpeg=true'
    '-Dpng=true'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection gir'
    'gtk-doc docs'
    'jpeg2000 jasper'
    'tiff tiff'
    'X x11'
)

gdk_pixbuf_cache_loaders() {
    #FIXME: adapt for multiarch
    case "${SLOT}" in
        2.0)
            echo "Generating pixbuf list ..."
            nonfatal edo gdk-pixbuf-query-loaders --update-cache
            if [[ -x /usr/i686-pc-linux-gnu/bin/gdk-pixbuf-query-loaders ]]; then
                nonfatal edo /usr/i686-pc-linux-gnu/bin/gdk-pixbuf-query-loaders --update-cache
            fi
        ;;
    esac
}

gdk-pixbuf_src_prepare(){
    meson_src_prepare
    edo ln -s "${WORKBASE}"/${PNV}/tests "${WORK}/"
}

gdk-pixbuf_pkg_postinst() {
    default
    gdk_pixbuf_cache_loaders
}

gdk-pixbuf_pkg_postrm() {
    default
    gdk_pixbuf_cache_loaders
}

