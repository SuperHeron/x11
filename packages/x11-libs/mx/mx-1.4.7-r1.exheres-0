# Copyright 2011 Paul Seidler
# Distributed under the terms of the GNU General Public License v2

require github [ user='clutter-project' suffix='tar.xz' ]

SUMMARY="Clutter based Toolkit for standard interface elements"
DESCRIPTION="
Mx is a widget toolkit using Clutter that provides a set of standard interface
elements, including buttons, progress bars, scroll bars and others. It also
implements some standard  managers. One other interesting feature is the
possibility setting style properties from a CSS format file."
HOMEPAGE="http://www.clutter-project.org/"

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64"
MYOPTIONS="
    dbus
    gesture   [[ description = [ Support for the gesture-recognition framework ] ]]
    gobject-introspection
    gtk       [[ description = [ Build the GTK+ widgets library ] ]]
    gtk-doc   [[ requires = gtk ]]
    imcontext [[ description = [ Support for the Input Method Framework ] ]]
    startup-notification
"


DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35]
        dev-util/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.4] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        dev-libs/glib:2[>=2.28]
        x11-apps/xrandr[>=1.2.0]
        x11-libs/clutter:1[>=1.7.91][gobject-introspection?]
        x11-libs/gdk-pixbuf:2.0[>=2.20]
        dbus? ( dev-libs/dbus-glib:1[>=0.82] )
        gesture? ( x11-libs/clutter-gesture )
        gtk? ( x11-libs/gtk+:2[>=2.20] )
        imcontext? ( x11-libs/clutter-imcontext[>=0.1] )
        startup-notification? ( x11-libs/startup-notification[>=0.9] )
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}/92e456ac34ddb1012751940f75ae2b02c514f1e9.patch" )

DEFAULT_SRC_CONFIGURE_PARAMS=( '--with-winsys=x11' '--without-glade' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
    'gtk gtk-widgets'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'dbus'
    'gesture clutter-gesture'
    'imcontext clutter-imcontext'
    'startup-notification'
)

