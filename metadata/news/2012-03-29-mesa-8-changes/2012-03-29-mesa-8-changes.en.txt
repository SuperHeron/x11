Title: Changes with Mesa-8
Author: Paul Seidler <sepek@exherbo.org>
Content-Type: text/plain
Posted: 2012-03-29
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: x11-dri/mesa[<8.0.2]

With Mesa-8 all DRI(-1)-only drivers were removed. Specifically, i810, mach64,
mga, r128, savage, sis, tdfx, and unichrome.

Because some drivers were dropped and to reflect better the the meanings of
the options some of them are renamed:
video_drivers:llvm            -> llvm
video_drivers:gallium-intel   -> video_drivers:i915
video_drivers:gallium-nouveau -> video_drivers:nouveau
video_drivers:gallium-radeon  -> video_drivers:radeon
video_drivers:nouveau         -> video_drivers:nouveau-legacy
video_drivers:radeon          -> video_drivers:radeon-legacy

For more information read the descriptions of the options.
